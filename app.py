from flask import request, Flask, render_template
app = Flask(__name__)

@app.route('/')
@app.route('/headers')
def header():
    headers = dict(request.headers) 
    print(headers)
    if headers:  
        return render_template('headers.html', headers=headers)
    else:
        headers = {'NULL':'NULL'}
        return render_template('headers.html', headers=headers)


@app.route('/body', methods=['GET', 'POST'])
def body():
    if request.method == 'POST':
        body = request.get_json(force=True)
    if not body:
        body = 'NULL'        
    return render_template('body.html', body=body)



if __name__ == '__main__':
    app.run('0.0.0.0', 8080)

